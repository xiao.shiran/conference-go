import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    header = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": city + " " + state,
        "per_page": 1
    }
    url = "https://api.pexels.com/v1/search"

    response = requests.get(url, params=params, headers=header)
    content = response.json()
    print(content["photos"][0]["src"]["original"])

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_lat_lon(city, state):
    params = {
        "q": city,
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1
    }
    url = "http://api.openweathermap.org/geo/1.0/direct?"
    response = requests.get(url, params=params)
    content = response.json()

    try:
        return {
            "lat": content[0]["lat"],
            "lon": content[0]["lon"],
            }
    except (KeyError, IndexError):
        return {"lat": None, "lon": None}


def get_weather_data(city, state):
    params = {
        "lat": get_lat_lon(city, state)["lat"],
        "lon": get_lat_lon(city, state)["lon"],
        "appid": OPEN_WEATHER_API_KEY
    }
    url = "https://api.openweathermap.org/data/2.5/weather?"
    response = requests.get(url, params=params)
    content = response.json()

    try:
        return {
            "weather": content["weather"][0]["main"],
            "temperature": content["main"]["temp"],
            }
    except (KeyError, IndexError):
        return {"weather": None, "temperature": None}
